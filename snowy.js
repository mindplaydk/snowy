(function () {

    console.log("SNOW")

    const GRAVITY = 4.5 // constant gravity (low values cause upward gusts of wind!)
    const WINDS = 3 // number of wind systems (higher number -> less clustering)
    const MAX_WIND = 4 // maximum wind strength
    const WIND_CHANGE = 0.03; // how much the wind changes over time
    const SIZE_MIN = 5 // minimum flake size
    const SIZE_RANGE = 8 // flake size variation range
    const COUNT = 250 // number of flakes (higher numbers work best with more wind systems)

    let flakes = []
    let winds = []

    for (let i = 0; i < WINDS; i++) {
        winds[i] = {
            dx: Math.random() * 2 - 1,
            dy: Math.random() * 2 - 1
        }
    }

    winds[WINDS] = winds[0] // wrap around from the last to the first wind system

    for (let i = 0; i < COUNT; i++) {
        let el = document.createElement('div')
        let size = Math.round(Math.random() * SIZE_RANGE + SIZE_MIN)

        el.style.backgroundColor = 'white'
        el.style.borderRadius = '50%'
        el.style.position = 'fixed'
        el.style.width = size + 'px'
        el.style.height = size + 'px'
        el.style.pointerEvents = 'none'
        el.style.zIndex = '1000'

        document.body.appendChild(el)

        flakes.push({
            x: Math.random() * innerWidth,
            y: Math.random() * innerHeight,
            wi: Math.random() * WINDS,
            el
        })
    }

    function clamp(num, min, max) {
        return num <= min ? min : num >= max ? max : num;
    }

    function paint() {
        // adjust wind speeds:

        for (var wind of winds) {
            wind.dx = clamp(wind.dx + Math.random() * 0.2 - 0.1, -MAX_WIND, MAX_WIND)
            wind.dy = clamp(wind.dy + Math.random() * 0.2 - 0.1, -MAX_WIND, MAX_WIND)
        }

        // apply winds to snow flakes:

        for (var flake of flakes) {
            flake.wi = (flake.wi + Math.random() * WIND_CHANGE + WINDS) % WINDS

            let wi = Math.floor(flake.wi)
            let wd = flake.wi % 1

            flake.x += winds[wi].dx * (1 - wd) + winds[wi + 1].dx * wd
            flake.y += winds[wi].dy * (1 - wd) + winds[wi + 1].dy * wd

            if (flake.x < -15) {
                flake.x = innerWidth + 10
                flake.y = Math.random() * 0.8 * innerHeight
            } else if (flake.x > innerWidth + 15) {
                flake.x = -10
                flake.y = Math.random() * 0.8 * innerHeight
            }

            flake.y += GRAVITY

            if (flake.y > innerHeight) {
                // restart snow flake:
                flake.x = Math.random() * innerWidth
                flake.y = -10
            }

            flake.el.style.left = Math.round(flake.x) + 'px'
            flake.el.style.top = Math.round(flake.y) + 'px'
        }

        requestAnimationFrame(paint)
    }

    paint()

})();
